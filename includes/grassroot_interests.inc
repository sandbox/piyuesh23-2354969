<?php

/**
 * @file
 * Get Search Interests From Keywords.
 */

use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Implements gets actual grassroot results.
 */
function grassroot_interests_search_interest_process($keyword, $disptext, $dispno) {
  $result = db_select('grassroot_interests_path_keyword', 'sipk');
  $result->fields('sipk', array('kw_title', 'root_url'));
  $result->distinct();
  $result->condition('sipk.keyword', $keyword);
  if ($dispno != 0) {
    $result->range(0, $dispno);
  }
  $output = $result->execute()->fetchAll();
  $attributes = array(
    'id' => 'my-interests-listing',
    'class' => 'list-messages',
  );
  // A string or indexed (string) array with the classes for the list tag.
  $count = 0;
  $data = array();
  foreach ($output as $key => $value) {
    $title = t('@keyword_title', array('@keyword_title' => $value->kw_title));
    $uri = Url::fromUri($value->root_url, array('attributes' => array('class' => 'search-interest-link')));
    $interests_link = \Drupal::l($title, $uri);
    $interests = t($disptext) . ' ' . $interests_link . '.';
    // Set custom attributes for a list item.
    $data[] = array(
      '#markup' => $interests,
      '#wrapper_attributes' => array(
        'id' => drupal_html_id($value->kw_title),
        'class' => array(drupal_html_class($value->kw_title), 'grassroot-interest-item'),
      ),
    );
    $count++;
  }
  $grass_interests = array(
    '#theme' => 'item_list',
    '#items' => $data,
    '#type' => 'ul',
    '#attributes' => $attributes,
    '#prefix' => '<div class="message-specialty" id="search-results-visit-specialty">',
    '#suffix' => '</div>',
  );
  return drupal_render($grass_interests);
}
